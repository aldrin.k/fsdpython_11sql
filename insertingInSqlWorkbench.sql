create database employee_10;
/* Write a query to create a table employee_data
(empname, empid, doj, dob, 
department['developers', 'Hr', 'ops', 'production'], 
age, salary.) */
create table employee_data (empname varchar(50) NOT null,
empid int not null,  doj date, dob date, 
departments varchar(20), age int, salary int);
insert into employee_data values
('Gokul', 1, '2020-10-04', '1991-08-09', 'Developer', 32, 1250000),
('Harish Jani', 2, '2021-09-05', '1990-06-14', 'Operation', 33, 650000),
('Harish B', 3, '2019-11-02', '1991-09-19', 'Developer', 32, 850000),
('Valmi', 4, '2022-07-24', '1999-12-20', 'Operation', 22, 420000),
('Madan', 5, '2021-05-01', '1987-12-04', 'HR', 36, 850000),
('Sai Lakshmi', 6, '2018-12-15', '2000-01-01', 'Developer', 23, 490000),
('Anjani', 7, '2020-10-01', '1994-08-27', 'HR', 29, 950000),
('Pravallika', 8, '2020-10-04', '2001-06-09', 'Production', 22, 370000),
('Gayathri', 9, '2018-12-15', '1984-06-12', 'Developer', 39, 1450000),
('Jayram', 10, '2022-07-24', '2000-09-09', 'Production', 22, 350000),
('Srikanth', 11, '2020-10-04', '1997-11-20', 'Production', 25, 750000);

