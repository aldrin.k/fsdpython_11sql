/* make two columns (empname,departments) as one column. */
select concat(empname, ' ', departments) as new_column from employee_data;
select concat(empname, ' ', departments) as new_column from employee_data limit 2;

/* order by */

select * from employee_data order by age DESC;
select * from employee_data order by age desc limit 4,1;

/* executing sequence From -> Select -> Order by */

select * from employee_data order by age desc limit 4,1;
select concat(empname,' ',departments) as new_column from employee_data order by departments;

select * from employee_data order by age asc, empname desc;

select empname, age from employee_data
order by empname asc, age desc;

/* where */

