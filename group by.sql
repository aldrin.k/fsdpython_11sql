/* Group by */

select distinct status from orders;
select distinct city from customers;

select * from customers;
select * from orders;

select status, count(status) from orders group by status;
select status, count(*) from orders group by status;
select city, count(city) as k from customers group by city order by k desc limit 1;
select jobtitle, count(jobtitle) from employees group by jobtitle;

select * from payments;
select customernumber,count(customernumber) from payments group by customerNumber; 
select customernumber,sum(amount) from payments group by customerNumber; 

select status, sum(quantityordered * priceEach) as amount 
from orders 
join orderdetails
using (ordernumber)
group by status;

select status, sum(quantityordered * priceEach) as amount 
from orders 
left join orderdetails
using (ordernumber)
group by status;

select status, sum(quantityordered * priceEach) as amount 
from orders 
right join orderdetails
using (ordernumber)
group by status;

select status, sum(quantityordered * priceEach) as amount 
from orders 
inner join orderdetails
using (ordernumber)
group by status;

select * from orders;

select orderNumber, sum(quantityordered * priceEach) as amount 
from orderdetails
group by orderNumber;

/* 2003, 2004, 2005 only shipped */ 

select year(orderdate) as year,
sum(quantityordered * priceEach) as amount 
from orders
join orderdetails
using (ordernumber)
group by year;

select year(orderdate) as year,
count(ordernumber)
from orders
where status = 'shipped'
group by year;

select state from customers group by state;
select distinct state from customers;

