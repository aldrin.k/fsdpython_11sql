-- Use classicmodels;
-- select * from customers;
-- select distinct state from customers;
-- select distinct state, city from customers where state is not null order by state;
-- select state, city from customers where state is not null order by state;

-- /* Join */
-- select * from products;
-- select * from productlines;

-- select productcode, productname from products t1 inner join 
-- productlines t2 on t1.productline = t2.productline;

-- select productcode, productname from products inner join productlines 
-- using (productline);

-- select t1.orderNumber,t1.status, 
-- sum(quantityordered * priceEach) Total 
-- from orders t1
-- inner join orderdetails t2 
-- on t1.ordernumber = t2.ordernumber
-- group by ordernumber;

-- select t1.orderNumber,t1.status,
-- sum(quantityordered * priceEach) Total 
-- from orders t1
-- inner join orderdetails t2 
-- on t1.ordernumber = t2.ordernumber
-- group by ordernumber;

-- -- t1 order-> ornumb, ordate,redate, shidate, status, comments, cusnumb
-- -- t2 orderdetails- ornumb, procode, qaun, priceeach, orderlinenumber
-- select * from products;
-- select * from orderdetails;

-- select productname, ordernumber, msrp, priceeach
-- from products p
-- inner join orderdetails o
-- on p.productcode = o.productcode
-- where p.productname = '1992 Ferrari 360 Spider red';

-- -- select * from products;
-- -- select * from orderdetails;
select * from orders;

select customernumber, productname 
-- from orderdetails
-- inner join products
-- using (productcode)
-- inner join orders
-- using (ordernumber)
-- order by customernumber desc;

-- select products.productname, orderdetails.productcode 
-- from products 
-- join orderdetails
-- where products.productname = 'American Airlines: MD-11S';

select customers.customerNumber,
customers.customername
from customers -- take full t1 partial t2
left join payments
using (customernumber);



select customers.customerNumber,
customers.customername
from customers -- common 
inner join payments
using (customernumber);

select customers.customerNumber,
customers.customername
from customers
right join payments -- t2 full partial t1
using (customernumber);

select * from orders;
-- customername, customernumber, ordernumber, status

select customers.customername,
customers.customernumber,
orders.ordernumber,
orders.status
from customers
left join orders
using (customernumber)
where ordernumber is null;

desc customers;
