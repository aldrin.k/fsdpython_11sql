use classicmodels;
select * from customers;

/* where */

select count(customerName) as total_fromUSA 
from customers where country = 'USA';
select * from customers where country = 'USA' 
limit 5 ;
select * from customers where country = 'USA' 
order by customerName limit 5 ;

/* executing sequence From -> Where -> Select -> Order by -> limit */

select * from employees;
select firstName, lastName, jobTitle 
from employees 
where jobtitle = 'Sales Rep';
select *
from employees 
where officeCode <= 3;
select *
from employees 
where officeCode = 1 or officeCode = 3;
select *
from employees 
where officeCode between 1 and 3;
select *
from employees 
where officeCode in (1, 2, 3);
select firstName, lastName, jobTitle 
from employees
where firstName like "%a";
select count(firstName)
from employees
where firstName like "a%";